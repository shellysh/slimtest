<?php
require "bootstrap.php";
use Chatter\Models\User;
use Chatter\Models\Product;
use Chatter\Middleware\Logging;

$app = new \Slim\App();
$app->add(new Logging());

$app->get('/users', function($request, $response,$args){
    $_user = new User();
    $users = $_user->all();

    $payload = [];
    foreach($users as $u){
        $payload[$u->id] = [
            "username" => $u->username,
            "email" => $u->email
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

//Q2
$app->get('/products', function($request, $response,$args){
    $_product = new Product();
    $products = $_product->all();

    $payload = [];
    foreach($products as $u){
        $payload[$u->id] = [
            "name" => $u->name,
            "price" => $u->price
        ];
    }
    return $response->withStatus(200)->withJson($payload);
});

//Q4
$app->put('/products/{product_id}', function($request, $response, $args){
    $name = $request->getParsedBodyParam('name','');
    $price = $request->getParsedBodyParam('price','');
    $_product = Product::find($args['product_id']);
    $_product->name = $name;
    $_product->price = $price;
   
    if($_product->save()){
        $payload = ['product_id' => $_product->id,"result" => "The product has been updates successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

$app->get('/products/{product_id}', function($request, $response,$args){
    $_id = $args['product_id'];
    $product = Product::find($_id);
    return $response->withStatus(200)->withJson($product);
});

//Q3
$app->get('/search/{name}', function($request, $response,$args){
    $_product = new Product();
    $name = $request->getAttribute('name');
    $products = $_product->all();

    $payload = [];
    foreach($products as $u){
        //if($u->name == ){
        if(strpos($u->name, $name) !== false){
            $payload[$u->id] = [
                "id" => $u->id,
                "name" => $u->name,
                "price" => $u->price
            ];
        }
    }
    return $response->withStatus(200)->withJson($payload);
    
});

$app->post('/users', function($request, $response,$args){
    $email = $request->getParsedBodyParam('email','');
    $username = $request->getParsedBodyParam('username','');
    $_user = new User();
    $_user->email = $email;
    $_user->username = $username;
    $_user->save();
       
    if($_user->id){
        $payload = ['user_id' => $_user->id];
        return $response->withStatus(201)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

$app->put('/users/{user_id}', function($request, $response, $args){
    $email = $request->getParsedBodyParam('email','');
    $username = $request->getParsedBodyParam('username','');
    $_user = User::find($args['user_id']);
    $_user->email = $email;
    $_user->username = $username;
   
    if($_user->save()){
        $payload = ['user_id' => $_user->id,"result" => "The user has been updates successfuly"];
        return $response->withStatus(200)->withJson($payload);
    }
    else{
        return $response->withStatus(400);
    }
});

$app->delete('/users/{user_id}', function($request, $response,$args){
    $user = User::find($args['user_id']);
    $user->delete();
    
    if($user->exists){
        return $response->withStatus(400);
    }
    else{
       return $response->withStatus(200);
   }
});

$app->post('/users/bulk', function($request, $response,$args){
    $payload = $request->getParsedBody();

    User::insert($payload);
    return $response->withStatus(201)->withJson($payload);
});

$app->options('/{routes:.+}', function ($request, $response, $args) {
    return $response;
});

$app->add(function ($req, $res, $next) {
    $response = $next($req, $res);
    return $response
            ->withHeader('Access-Control-Allow-Origin', '*')
            ->withHeader('Access-Control-Allow-Headers', 'X-Requested-With, Content-Type, Accept, Origin, Authorization')
            ->withHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
});

$app->run();